#![warn(clippy::all, clippy::pedantic, clippy::nursery)]

use ::next_gen::prelude::*;
use indicatif::{ParallelProgressIterator, ProgressBar, ProgressStyle};
use maplit::btreemap;
use plotlib::page::Page;
use plotlib::repr::{Histogram, HistogramBins};
use plotlib::view::ContinuousView;
use rand::distributions::Uniform;
use rand::rngs::SmallRng;
use rand::{Rng, SeedableRng};
use rayon::prelude::*;
use statistical::{mean, median, standard_deviation};
use std::collections::HashSet;

fn make_rng() -> impl Iterator<Item = u64> {
    const N_BONDS: u64 = 98_952_302_605;

    let bond_range = Uniform::new_inclusive(1, N_BONDS);
    let rng = SmallRng::from_entropy();
    rng.sample_iter(bond_range)
}

#[generator((u64, u64))]
fn draw_prizes(mut random_bonds: impl Iterator<Item = u64>) {
    // Checking if a bond wins multiple times is costly and makes little
    // difference, so do not bother by default
    const CHECK_MULTIPLE_DRAWS: bool = false;

    // Map relating prize amounts in £ to number of prizes of that amount
    let prizes = btreemap! {
        1_000_000 => 2,
        100_000 => 4,
        50_000 => 9,
        25_000 => 17,
        10_000 => 42,
        5_000 => 86,
        1_000 => 1648,
        500 => 4_944,
        100 => 26_795,
        50 => 26_795,
        25 => 2_807_840,
    };

    let mut previous_winners = HashSet::<u64>::new();
    let mut _extra_draws: u64 = 0;

    // Iterate in reverse: we want to assign the higher value prizes first as
    // a bond drawn multiple times will only win the greater prize
    for (value, n_prizes) in prizes.into_iter().rev() {
        let mut prizes_awarded: u64 = 0;

        while prizes_awarded < n_prizes {
            let winning_bond = random_bonds.next().unwrap();

            // bond cannot win twice
            if CHECK_MULTIPLE_DRAWS && previous_winners.contains(&winning_bond) {
                _extra_draws += 1;
                continue;
            } else {
                prizes_awarded += 1;
                yield_!((winning_bond, value));

                if CHECK_MULTIPLE_DRAWS {
                    previous_winners.insert(winning_bond);
                }
            }
        }
    }
}

fn invest(mut investment: u64) -> u64 {
    const N_MONTHS: u64 = 12;
    let mut total_winnings: u64 = 0;
    let mut random_bonds = make_rng();

    for _ in 1..=N_MONTHS {
        // Up to £50,000 can be invested
        if investment > 50_000 {
            investment = 50_000;
        }

        mk_gen!(let winning_bonds = draw_prizes(random_bonds.by_ref()));
        let winnings: u64 = winning_bonds
            .into_iter()
            .filter(|x| x.0 < investment) // we have bond numbers 1..=investment
            .map(|x| x.1)
            .sum();

        investment += winnings;
        total_winnings += winnings;
    }

    total_winnings
}

fn simulate_invesmtents(investment: u64, n_simulations: u64) -> Vec<f64> {
    let pb = ProgressBar::new(n_simulations);
    pb.set_style(
        ProgressStyle::default_bar()
            .template("[{elapsed_precise}] {bar:40.cyan/blue} ({pos}/{len}, ETA {eta})"),
    );

    (0..n_simulations)
        .into_par_iter()
        .progress_with(pb)
        .map(|_| ((invest(investment) + investment) as f64 / investment as f64 - 1.) * 100.)
        .collect::<Vec<f64>>()
}

pub fn run() {
    const INVESTMENT: u64 = 20_000;
    const N_SIMULATIONS: u64 = 10_000;
    const HISTO_MAX: u64 = 4;

    let aprs = simulate_invesmtents(INVESTMENT, N_SIMULATIONS);
    let h = Histogram::from_slice(
        &aprs
            .iter()
            .cloned()
            .filter(|x| x <= &(HISTO_MAX as f64)) // panics with any value greater than final bin edge
            .collect::<Vec<_>>()[..],
        HistogramBins::Bounds(
            (0..=2 * HISTO_MAX)
                .map(|x| x as f64 / 2.)
                .collect::<Vec<_>>(),
        ),
    );
    let v = ContinuousView::new().add(h);

    let mean = mean(&aprs);
    // println!("{:?}", aprs);
    println!("Median APR earned is {:.2}%", median(&aprs));
    println!("Mean APR earned is {:.2}%", mean);
    println!(
        "Standard deviation is {:.2}",
        standard_deviation(&aprs, Some(mean))
    );
    println!(
        "Smallest APR earned was {:.2}%",
        aprs.iter()
            .min_by(|a, b| a.partial_cmp(b).unwrap())
            .unwrap()
    );
    println!(
        "Greatest APR earned was {:.2}%",
        aprs.iter()
            .max_by(|a, b| a.partial_cmp(b).unwrap())
            .unwrap()
    );
    println!("");
    println!(
        "{}",
        Page::single(&v).dimensions(120, 30).to_text().unwrap()
    );
}
