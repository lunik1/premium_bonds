#![warn(clippy::all, clippy::pedantic, clippy::nursery)]

fn main() {
    premium_bonds::run();
}
